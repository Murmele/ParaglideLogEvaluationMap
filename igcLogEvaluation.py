#import matplotlib.pyplot as plt
#import matplotlib
#matplotlib.use('TkAgg')
from readIGC import readLogFiles, readLogFilesList
#import datetime
from colormap import createColormap
#from threeDScatter import create3DScatter
import sys, os, getopt
from urllib.parse import urlparse
import xcontest
import pathlib

# example: python igcLogEvaluation.py "../Paragleiten"

if __name__ == "__main__":
    num_arguments = len(sys.argv)
    if num_arguments < 2:
        raise Exception("Please provide a path, an igc logfile or a link to a xcontest flight")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["filter", "delta_t=", "num_data_points="])
    except getopt.GetoptError:
        print("python igcLogEvaluation.py [options] <paths>")
        sys.exit(2)

    options = {}
    for opt, arg in opts:
        if opt == "--filter":
            options["filter"] = True
        elif opt == "--delta_t":
            delta_t = float(arg)
            options["delta_t"] = delta_t
        elif opt == "--num_data_points":
            num = int(arg)
            options["num_data_points"] = num

    if "delta_t" in options and "filter" not in options:
        print("delta_t gets ignored, because filtering is disabled.")

    if "num_data_points" in options and "filter" not in options:
        print("num_data_points gets ignored, because filtering is disabled")

    if "delta_t" in options and "num_data_points" in options:
        print("num_data_points gets ignored, because delta_t is used")

    file_path = pathlib.Path(__file__).parent.absolute()
    xc = None
    xcontest_credentials_file = os.path.join(file_path, "xcontest_username_password.txt")
    if os.path.isfile(xcontest_credentials_file):
        with open(xcontest_credentials_file, "r") as f:
            username = f.readline().strip()
            password = f.readline().strip()
        xc = xcontest.XContest(username, password)

    log_files = []
    paths = []
    for path in args:
        parsed_url = urlparse(path)
        if parsed_url.scheme and parsed_url.netloc:
            # valid url
            if xc is None:
                raise Exception("No xc credentials available. Therefore parsing evaluating xc data is not possible. Please create a 'xcontest_username_password.txt' with the username in the first row and the password in the second row and execute the script again")
            log_files.append(xc.get_igc(path))  # download igc file in temporary folder and add it to paths
        else:
            (_, extension) = os.path.splitext(path)
            if extension and extension in [".IGC", ".igc"]:
                # valid igc file
                log_files.append(path)
            elif not extension:
                # folder
                paths.append(path)

    datasets = readLogFiles(paths, options)
    datasets.extend(readLogFilesList(log_files, options))

    if len(datasets) == 0:
        raise Exception("No logs found.")

    createColormap(datasets)
