import requests
import time
import os
import pathlib
import re
import json
import tempfile


class XContestError(Exception):
    pass


class XContest():
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.session = None
        self.timeout_s = 5  # timeout of the session [s]
        self.max = 2
        self.authentication_time = 0
        #self.authenticate()

    def authenticate(self):
        if self.session is not None:
            self.session.close()

        self.session = requests.Session()
        self.session.headers.update({'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64)"})

        payload = {'login[username]': self.username,
                   'login[password]': self.password}
        response = self.session.post("https://www.xcontest.org/world/en/", data=payload)  # do login
        if not response.ok:
            self.session.close()
            self.session = None
            print("XContest Warning: login was not successful")
            return False
        if "Warning" in response.headers:
            if response.headers["Warning"] == "Login":
                raise XContestError("Login to XContest failed.")
            else:
                raise XContestError(f"Unable to login: {response.headers['Warning']}")

        if "Keep-Alive" in response.headers:
            keep_alive = response.headers["Keep-Alive"].split(",")
            if len(keep_alive) > 1:
                timeout_l = keep_alive[0].strip().split("=")
                self.timeout_s = 2  # default value (quite common)
                if len(timeout_l) > 1:
                    self.timeout_s = int(timeout_l[1])
                max_l = keep_alive[1].strip().split("=")
                self.max = 1
                if len(max_l) > 1:
                    self.max = int(max_l[1])
        self.authentication_time = time.time()
        return True

    def get_flight_information(self, url):
        if time.time() - self.authentication_time > self.timeout_s or self.session is None:
            if not self.authenticate():  # renew connection because of timeout it was closed
                raise XContestError("Unable to get flight information")

        # 1) Find flight id which is needed for the API
        response = self.session.get(url)
        if not response.ok:
            raise XContestError("Unable to download page")

        pattern = "app.addFlight\((?P<id>[0-9]*)\)"
        pattern = re.compile(pattern)
        m = pattern.search(response.text)
        if m is None:
            raise XContestError("Seems not to be a xcontest flight")

        id = m["id"]

        # 2) Download flight information
        ZenControllerApiKey = "03ECF5952EB046AC-A53195E89B7996E4-D1B128E82C3E2A66"
        url = f"https://www.xcontest.org/api/data/?flights3/world/2023:{id}&lng=en&key={ZenControllerApiKey}"
        response = self.session.get(url)
        if not response.ok:
            raise XContestError("Unable to download data")
        r = json.loads(response.text)

        if "error" in r:
            raise XContestError("Unable to download flight data")
        return r

    def get_igc(self, url):
        r = self.get_flight_information(url)

        if "igc" not in r:
            raise XContestError("No igc file in flight information")

        if "link" not in r["igc"]:
            raise XContestError("No igc url in flight information")

        igc_url = r["igc"]["link"]
        ident = r["ident"]
        # ident is important, because otherwise it is not possible to download
        referer = f"https://www.xcontest.org/world/en/flights/detail:{ident}"
        self.session.headers.update({'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64)", 'Referer': referer})
        response = self.session.get(igc_url)
        if not response.ok:
            raise XContestError("Unable to download igc file")

        f = tempfile.NamedTemporaryFile(delete=False)
        f.write(response.text.encode('ascii'))

        return f.name


if __name__ == "__main__":
    file_path = pathlib.Path(__file__).parent.absolute()
    xc = None
    xcontest_credentials_file = os.path.join(file_path, "xcontest_username_password.txt")
    if os.path.isfile(xcontest_credentials_file):
        with open(xcontest_credentials_file, "r") as f:
            username = f.readline().strip()
            password = f.readline().strip()
        xc = XContest(username, password)

        print(f'Igc file stored in: {xc.get_igc("https://www.xcontest.org/world/en/flights/detail:Murmele/7.4.2023/10:37")}')
